import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:weather/base/base_view_model.dart';
import 'package:weather/entity/coordinate.dart';
import 'package:weather/entity/language.dart';
import 'package:weather/entity/weather.dart';
import 'package:weather/localizations/language_provider.dart';
import 'package:weather/network/event_result.dart';
import 'package:weather/repository/base_repository.dart';
import 'package:weather/repository/language_repository.dart';
import 'package:weather/repository/weather_repository.dart';

class MainViewModel extends BaseViewModel {
  final weatherRepo = WeatherRepository.getSingleton();
  final languageRepo = LanguageRepository.getSingleton();

  @override
  List<BaseRepository> get repoList => [];
  var getCoordinateApiState, getCurrentWeatherApiState, getMultilingualApiState;
  Weather? weather;
  List<String> cityList = [
    "Kuala Lumpur",
    "George Town",
    "Klang",
    "Ipoh",
    "Butterworth",
    "Johor Bahru",
    "Petaling Jaya",
    "Kuantan",
    "Shah Alam",
    "Kota Bharu",
    "Melaka",
    "Kota Kinabalu",
    "Seremban",
    "Sandakan",
    "Sungai Petani",
    "Kuching",
    "Kuala Terengganu",
    "Alor Setar",
    "Putrajaya",
    "Subang Jaya",
    "Cheras",
    "Ampang",
    "Malacca City",
    "Kulai",
    "Selayang",
    "Tawau",
    "Taiping",
    "Miri",
    "Muar",
    "Bintulu",
    "Kluang",
    "Sibu",
    "Kangar",
    "Labuan"
  ];
  getCoordinate(String cityName, Locale? locale) async {
    setLoading();
    getCoordinateApiState = await weatherRepo.getCoordinate(cityName);
    if (getCoordinateApiState!.isSuccess) {
      Coordinate _coordinate = getCoordinateApiState.result.first;
      Language lang= LanguageProvider().langList.singleWhere((e) => e.locale == locale);
      getCurrentWeather(_coordinate.lat.toString(), _coordinate.lon.toString(), lang.code);
    }
    setLoading(false);
  }


  getCurrentWeather(String lat, String lon, String langCode) async {
    setLoading();
    getCurrentWeatherApiState = await weatherRepo.getCurrentWeather(lat, lon,langCode);
    if (getCurrentWeatherApiState!.isSuccess) {
      weather = getCurrentWeatherApiState.result;
    }
    setLoading(false);
  }


}
