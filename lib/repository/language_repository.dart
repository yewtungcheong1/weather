import 'package:weather/entity/coordinate.dart';
import 'package:weather/entity/http_method.dart';
import 'package:weather/entity/weather.dart';
import 'package:weather/network/api_request_builder.dart';
import 'package:weather/network/event_result.dart';
import 'package:weather/repository/base_repository.dart';

class LanguageRepository extends BaseRepository {
  static LanguageRepository? _singleton;

  static LanguageRepository getSingleton() {
    _singleton ??= LanguageRepository();
    return _singleton!;
  }


  Future<EventResult<List<Coordinate>>> getMultilingual(String lat, String lon, String lang) async {
    return await ApiRequestBuilder<List<Coordinate>>(
      method: HttpMethod.GET,
      endPoint: "data/2.5/weather",
      params: {
        "lat":lat,
        "lon":lon,
        "lang":lang,
        "appid": appId,
      },
      converter: (json) => (json as List<dynamic>).map((e) => Coordinate.fromJson(e)).toList(),
    ).execute();
  }
}
