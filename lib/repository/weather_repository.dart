import 'package:weather/entity/coordinate.dart';
import 'package:weather/entity/http_method.dart';
import 'package:weather/entity/weather.dart';
import 'package:weather/network/api_request_builder.dart';
import 'package:weather/network/event_result.dart';
import 'package:weather/repository/base_repository.dart';

class WeatherRepository extends BaseRepository {
  static WeatherRepository? _singleton;

  static WeatherRepository getSingleton() {
    _singleton ??= WeatherRepository();
    return _singleton!;
  }

  String appId = "d35e71e7017dddc67494ff5c1d5dc15a";

  Future<EventResult<Weather>> getCurrentWeather(String lat, String lon, String langCode) async {
    return await ApiRequestBuilder<Weather>(
      method: HttpMethod.GET,
      endPoint: "data/2.5/weather",
      params: {
        "lat": lat,
        "lon": lon,
        "lang":langCode,
        "appid": appId,
      },
      converter: (json) => Weather.fromJson(json),
    ).execute();
  }

  Future<EventResult<List<Coordinate>>> getCoordinate(String cityName) async {
    return await ApiRequestBuilder<List<Coordinate>>(
      method: HttpMethod.GET,
      endPoint: "geo/1.0/direct",
      params: {
        "q": cityName + ",MY",
        "appid": appId,
      },
      converter: (json) => (json as List<dynamic>).map((e) => Coordinate.fromJson(e)).toList(),
    ).execute();
  }
}
