import 'package:flutter/material.dart';

class BaseRepository extends ChangeNotifier {
  String appId = "d35e71e7017dddc67494ff5c1d5dc15a";

  setRepoValues(Function set) {
    set();
    notifyListeners();
  }

}