import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:weather/entity/http_method.dart';
import 'package:weather/network/services/api_service.dart';

import 'event_result.dart';

typedef T ResponseResultConverter<T>(dynamic);
typedef JwtRefresher = Future Function();
typedef void ResultConsumer(T);

class ApiRequestBuilder<T> extends ChangeNotifier {
  bool isAuthorization;
  bool isJWTAuthRequest;
  HttpMethod method;
  String? endPoint;

  dynamic params;
  ResponseResultConverter<T>? converter;
  int retryCounter = 0;
  JwtRefresher? jwtRefresher;
  ResultConsumer? consumer;
  dynamic imageContent;
  int timeout;
  bool isAssetUrl;
  bool isAutoHeader;
  bool isWithoutBaseResponse;
  bool isFeatures;

  ApiRequestBuilder({this.isAuthorization = true,
    this.isJWTAuthRequest = false,
    this.method = HttpMethod.POST,
    this.endPoint,
    this.params,
    this.converter,
    this.jwtRefresher,
    this.consumer,
    this.imageContent,
    this.timeout = 5,
    this.isAssetUrl = false,
    this.isAutoHeader = true,
    this.isWithoutBaseResponse = false,
    this.isFeatures = false});

  Future<EventResult<T>> execute() async {
    var response = await api(
      method,
      endPoint,
      content: params,
      timeout: timeout,
      isAssetUrl: isAssetUrl,
      isAutoHeader: isAutoHeader,
    );
    var result = await resolveBaseResponse(response, isWithoutBaseResponse, converter, consumer, isFeatures);
    return result;
  }

  Future<EventResult<T>> resolveBaseResponse<T>(http.Response response, bool isWithoutBaseResponse,
      [ResponseResultConverter<T>? converter, void Function(T)? onSuccess, bool? isFeatures]) async {
    if (response.statusCode == 200) {
      var value = utf8.decode(response.bodyBytes);
      var result = converter == null ? value as T : converter(json.decode(value));
      if (onSuccess != null) onSuccess(result);
      return EventResult.success(result);
    }
    else {
      return EventResult.failure();
    }
  }
}
