import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:weather/base/base_state.dart';
import 'package:weather/custom/custom_picker.dart';
import 'package:weather/entity/language.dart';
import 'package:weather/extension/converter.dart';
import 'package:weather/localizations/language_provider.dart';
import 'package:weather/localizations/my_localizations.dart';
import 'package:weather/main_view_model.dart';
import 'package:weather/extension/screen_apis.dart';
import 'package:weather/provider/provider_widget.dart';
import 'package:intl/intl.dart';
import 'package:weather/extension/string_extension.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();

  static void setLocale(BuildContext context, Locale newLocale) async {
    _MyAppState? state = context.findAncestorStateOfType<_MyAppState>();
    state!.setLocale(newLocale);
  }
}

class _MyAppState extends BaseState<MyApp> with WidgetsBindingObserver {
  Locale? _locale;

  setLocale(Locale locale) {
    setState(() {
      _locale = locale;
    });
  }

  _getLocale() {
    LanguageProvider.getSingleton().getLocale().then((locale) {
      setState(() {
        _locale = locale;
      });
    });
  }

  @override
  void didChangeDependencies() {
    _getLocale();
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      localizationsDelegates: const [
        MyLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate
      ],
      supportedLocales: LanguageProvider.getSingleton().langList.map((e) => e.locale).toList(),
      locale: _locale,
      localeResolutionCallback: (locale, supportedLocales) {
        for (var supportedLocale in supportedLocales) {
          if (supportedLocale.languageCode == locale!.languageCode && supportedLocale.scriptCode == locale.scriptCode) {
            return supportedLocale;
          }
        }
        return supportedLocales.first;
      },
      home: MyHomePage(locale: _locale),
    );
  }
}

class MyHomePage extends StatefulWidget {
  final Locale? locale;

  const MyHomePage({Key? key, this.locale}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends BaseState<MyHomePage> {
  final MainViewModel _vm = MainViewModel();
  String? currentCity;
  final GlobalKey<ScaffoldState> _key = GlobalKey();
  final RefreshController _refreshController = RefreshController();
  List<Language> list = LanguageProvider.getSingleton().langList;

  @override
  void initState() {
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      Duration(minutes: 1);
      _vm.getCoordinate(_vm.cityList.first, widget.locale);
    });
    super.initState();
  }

  @override
  void didChangeDependencies() {
    getCurrentLanguage();
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return ProviderWidget<MainViewModel>(
        model: _vm,
        builder: (ctx, vm, _) {
          _handleApiState();
          return Scaffold(
            key: _key,
            appBar: AppBar(
              backgroundColor: Color(0xff6082B6),
                title: Text(
                  getTranslatedText("weather"),
                ),
                actions: [
                  IconButton(
                    icon: const Icon(Icons.language),
                    onPressed: () => _key.currentState!.openEndDrawer(),
                  ),
                ]),
            endDrawer: Drawer(
              child: ListView(
                padding: EdgeInsets.zero,
                children: [
                  SizedBox(
                    height: 100.auto,
                    child: const DrawerHeader(
                      decoration: BoxDecoration(
                        color: Color(0xff6495ED),
                      ),
                      child: Text('Language'),
                    ),
                  ),
                  _buildLanguageListTile("English", "en"),
                  _buildLanguageListTile("中文", "zh"),
                ],
              ),
            ),
            body: _vm.weather == null
                ? buildProgressDialog()
                : Container(
                    width: context.screenWidth,
                    height: context.screenHeight,
                    decoration: const BoxDecoration(
                      gradient: LinearGradient(
                        colors: [
                          Color(0xff6082B6),
                          Color(0xff6495ED),
                        ],
                        begin: Alignment.bottomCenter,
                        end: Alignment.topCenter,
                      ),
                    ),
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 20.auto),
                      child: SmartRefresher(
                        controller: _refreshController,
                        enablePullDown: true,
                        enablePullUp: false,
                        onRefresh: () async {
                          await _vm.getCoordinate(currentCity ?? _vm.cityList.first, widget.locale ?? Locale("en", ""));
                          _refreshController.refreshCompleted();
                        },
                        child: ListView(
                          //controller: _scrollController,
                          children: <Widget>[
                            _buildCitiesListCard(),
                            _buildDate(),
                            _buildWeatherIcon(),
                            _buildTemperature(),
                            _buildTemperatureDetails(),
                          ],
                        ),
                      ),
                    ),
                  ),
            // ),
          );
        });
  }

  _buildLanguageListTile(String languageText, String code) {
    return ListTile(
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(languageText),
          Visibility(
            visible: getCurrentLanguage().code == code,
            child: const Icon(
              Icons.check,
              color: Color(0xff6495ED),
            ),
          ),
        ],
      ),
      onTap: () => _changeLanguage(code),
    );
  }

  _buildDate() {
    var currentDate = DateFormat('yyyy/MM/dd, hh:mm a').format(DateTime.now());
    return Container(
      alignment: Alignment.centerLeft,
      padding: EdgeInsets.only(bottom: 30.auto),
      child: Text(
        currentDate,
        style: TextStyle(fontSize: 15.auto, color: Colors.black),
      ),
    );
  }

  _buildWeatherIcon() {
    var imagePath = _getImagePath();
    return Image.asset(
      "images/weather_icon/$imagePath.png",
      height: context.screenWidth / 1.8,
    );
  }

  _getImagePath() {
    var imagePath = _vm.weather?.weatherDescription?.first.icon;
    if (imagePath!.contains("03")) {
      return "03";
    } else if (imagePath.contains("04")) {
      return "04";
    } else if (imagePath.contains("09")) {
      return "09";
    } else if (imagePath.contains("11")) {
      return "11";
    } else if (imagePath.contains("13")) {
      return "13";
    } else if (imagePath.contains("50")) {
      return "50";
    } else {
      return imagePath;
    }
  }

  _buildTemperature() => Container(
        alignment: Alignment.center,
        margin: EdgeInsets.only(top: 20.auto),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              (_vm.weather?.temperatureDetails?.temp ?? 0).kevinToCelsius(),
              style: TextStyle(fontSize: 90.auto, color: Colors.white),
            ),
            Text(
              getTranslatedText("feels_like") + (_vm.weather?.temperatureDetails?.feelsLike ?? 0).kevinToCelsius(),
              style: TextStyle(fontSize: 14.auto, color: Colors.white),
            ),
            Text(
              (_vm.weather?.weatherDescription?.first.description ?? "").capitalize(),
              style: TextStyle(fontSize: 14.auto, color: Colors.white),
            ),
          ],
        ),
      );

  _buildTemperatureDetails() => Container(
        margin: EdgeInsets.symmetric(vertical: 20.auto),
        child: GridView.count(
          crossAxisCount: 2,
          childAspectRatio: 1.5,
          shrinkWrap: true,
          physics: const AlwaysScrollableScrollPhysics(),
          crossAxisSpacing: 10,
          mainAxisSpacing: 10,
          children: [
            _buildTemperatureDetailsIcon(
                "pressure", "${(_vm.weather?.temperatureDetails?.pressure ?? 0).toStringAsFixed(0)} hPa"),
            _buildTemperatureDetailsIcon(
                "humidity", "${(_vm.weather?.temperatureDetails?.humidity ?? 0).toStringAsFixed(0)} %"),
            _buildTemperatureDetailsIcon("wind", "${_vm.weather?.wind?.speed ?? 0} m/s"),
            Visibility(
              visible: _vm.weather?.rain != null,
              child: _buildTemperatureDetailsIcon("rain_volume", "${_vm.weather?.rain?.oneHour ?? 0} mm"),
            )
          ],
        ),
      );

  _buildTemperatureDetailsIcon(String imagePath, String des) => Column(
        children: [
          _buildTemperatureDetailsImageIcon(imagePath),
          Text(
            des,
            style: TextStyle(fontSize: 14.auto, color: Colors.white),
          ),
        ],
      );

  _buildTemperatureDetailsImageIcon(String imagePath) => Container(
        padding: EdgeInsets.all(15.auto),
        margin: EdgeInsets.only(bottom: 10.auto),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.white,
        ),
        child: Image.asset("images/temp_icon/$imagePath.png", width: context.screenWidth / 8),
      );

  _buildCitiesListCard() => Container(
        padding: EdgeInsets.only(top: 15.auto, bottom: 15.auto),
        child: CustomPicker(
          title: getTranslatedText("city"),
          displayText: currentCity ?? _vm.cityList.first,
          list: _vm.cityList,
          onClick: (city) => _onSelectedCity(city),
        ),
      );

  _onSelectedCity(String city) {
    _vm.getCoordinate(city, widget.locale);
    setState(() {
      currentCity = city;
    });
  }

  _changeLanguage(String languageCode) async {
    var list = LanguageProvider.getSingleton().langList;
    Locale? _locale = await LanguageProvider.getSingleton().setLocale(list.singleWhere((e) => e.code == languageCode));
    MyApp.setLocale(context, _locale!);
    _vm.getCoordinate(currentCity ?? _vm.cityList.first, _locale);
  }

  _handleApiState() {
    handleApiState(_vm.getCurrentWeatherApiState, isAutoHideLoading: false, onSuccess: () {});
    handleApiState(_vm.getCoordinateApiState, isAutoHideLoading: false, onSuccess: () {});
  }
}
