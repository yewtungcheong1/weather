import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:weather/base/base_state.dart';
import 'package:weather/extension/screen_apis.dart';

class CustomPicker extends StatefulWidget {
  final String title;
  final String displayText;
  final List<dynamic> list;
  final ValueChanged<String> onClick;

  CustomPicker({
    Key? key,
    required this.title,
    required this.displayText,
    required this.list,
    required this.onClick,
  }) : super(key: key);

  @override
  _CustomPickerState createState() => _CustomPickerState();
}

class _CustomPickerState extends BaseState<CustomPicker> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(bottom: 5.auto, left: 5.auto),
          alignment: Alignment.topLeft,
          child: Text(
            widget.title,
            style: TextStyle(fontSize: 12.auto, color: Colors.black),
          ),
        ),
        Card(
          semanticContainer: true,
          clipBehavior: Clip.antiAlias,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          elevation: 5,
          child: DropdownButton2(
            isExpanded: true,
            hint: Text(widget.displayText),
            underline: Container(),
            items: widget.list.map((value) {
              return DropdownMenuItem(
                value: value,
                child: Text(
                  value,
                  style: const TextStyle(color: Colors.black),
                ),
              );
            }).toList(),
            onChanged: (value) => widget.onClick(value as String),
            style: const TextStyle(
              color: Colors.black,
            ),
            buttonPadding: const EdgeInsets.only(left: 20, right: 10),
            dropdownMaxHeight: 350.auto,
            dropdownDecoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.white,
            ),
          ),
        ),
      ],
    );
  }
}
