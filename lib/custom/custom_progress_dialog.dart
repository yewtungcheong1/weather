import 'package:ndialog/ndialog.dart';
import 'package:weather/extension/screen_apis.dart';
import 'package:flutter/material.dart';

CustomProgressDialog? _progressDialog;

class ProgressDialog {
  ProgressDialog.show(BuildContext context, String? url) {
    create(context, url);

    _progressDialog!.show();
  }

  ProgressDialog.hide() {
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      if (_progressDialog != null) _progressDialog!.dismiss();
    });
  }

  create(BuildContext context, String? url) {
    _progressDialog = CustomProgressDialog(context, blur: 2, dismissable: false);
    _progressDialog!.setLoadingWidget(
      const CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(
          Color(0xff6082B6),
        ),
      ),
    );
  }
}
