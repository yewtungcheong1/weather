extension Converter on double{
  String kevinToCelsius(){
    return (this - 273.15).toStringAsFixed(0)+"°C";
  }
}