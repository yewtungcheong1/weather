extension StringExtension on String {
  String capitalize() {
    if(isEmpty) {
      return "";
    } else {
      return "${this[0].toUpperCase()}${substring(1).toLowerCase()}";
    }
  }
}