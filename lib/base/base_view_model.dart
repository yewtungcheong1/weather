import 'package:flutter/material.dart';
import 'package:weather/repository/base_repository.dart';

abstract class BaseViewModel extends ChangeNotifier{
  List<BaseRepository> get repoList;
  var changeNotifierListener;
  var isLoading = false;

  BaseViewModel(){
    changeNotifierListener = (){
      notifyListeners();
    };
    repoList.forEach((repo) {
      repo.addListener(changeNotifierListener);
    });
  }

  @override
  void dispose() {
    for (var repo in repoList) {
      repo.removeListener(changeNotifierListener);
    }
    super.dispose();
  }

  setLoading([bool isLoading = true]){
    this.isLoading = isLoading;
    notifyListeners();
  }


}