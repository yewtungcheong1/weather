import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:weather/entity/language.dart';
import 'package:weather/localizations/language_provider.dart';
import 'package:weather/network/event_result.dart';

abstract class BaseState<T extends StatefulWidget> extends State<T> {
  handleApiState(
    EventResult? eventResult, {
    required Function onSuccess,
    Function? onError,
    bool isAutoHideLoading = true,
    bool isRequireResult = false,
    bool isCheckSessionExpired = true,
    bool isSplashScreen = false,
    bool isLogoutApiState = false,
    bool isMaintenanceDialog = false,
    Function? onLogout,
  }) async {
    if (eventResult == null) return;
    if (!eventResult.isHandled) {
      if (eventResult.isSuccess) {
        //if (isAutoHideLoading) ProgressDialog.hide();
        if (isRequireResult) {
          onSuccess(eventResult.result);
        } else {
          onSuccess();
        }
      }
    }
    eventResult.isHandled = true;
  }

  buildProgressDialog() => const Center(
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(
            Color(0xff6082B6),
          ),
        ),
      );

  getTranslatedText(String? key) {
    return LanguageProvider.getSingleton().getTranslated(
      context,
      key,
    );

  }

  Language getCurrentLanguage() {
    var list = LanguageProvider.getSingleton().langList;
    Language? deviceLanguage;

    deviceLanguage = list.singleWhere((e) => e.locale == Localizations.localeOf(context));
    return deviceLanguage;
  }
}
