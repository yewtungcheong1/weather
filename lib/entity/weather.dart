import 'package:json_annotation/json_annotation.dart';
import 'package:weather/entity/clouds.dart';
import 'package:weather/entity/rain.dart';
import 'package:weather/entity/snow.dart';
import 'package:weather/entity/sun.dart';
import 'package:weather/entity/temperature_details.dart';
import 'package:weather/entity/weather_description.dart';
import 'package:weather/entity/wind.dart';

part 'weather.g.dart';

@JsonSerializable()
class Weather {
  Weather(this.weatherDescription, this.temperatureDetails, this.wind, this.clouds, this.rain, this.snow, this.sun);

  @JsonKey(name: 'weather')
  List<WeatherDescription>? weatherDescription;
  @JsonKey(name: 'main')
  TemperatureDetails? temperatureDetails;
  Wind? wind;
  Clouds? clouds;
  Rain? rain;
  Snow? snow;
  @JsonKey(name: 'sys')
  Sun? sun;

  factory Weather.fromJson(Map<String, dynamic> json) => _$WeatherFromJson(json);

  Map<String, dynamic> toJson() => _$WeatherToJson(this);
}
