// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'temperature_details.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TemperatureDetails _$TemperatureDetailsFromJson(Map<String, dynamic> json) =>
    TemperatureDetails(
      (json['temp'] as num?)?.toDouble(),
      (json['feels_like'] as num?)?.toDouble(),
      (json['temp_min'] as num?)?.toDouble(),
      (json['temp_max'] as num?)?.toDouble(),
      (json['pressure'] as num?)?.toDouble(),
      (json['humidity'] as num?)?.toDouble(),
    );

Map<String, dynamic> _$TemperatureDetailsToJson(TemperatureDetails instance) =>
    <String, dynamic>{
      'temp': instance.temp,
      'feels_like': instance.feelsLike,
      'temp_min': instance.tempMin,
      'temp_max': instance.tempMax,
      'pressure': instance.pressure,
      'humidity': instance.humidity,
    };
