import 'package:json_annotation/json_annotation.dart';

part 'sun.g.dart';

@JsonSerializable()
class Sun {
  Sun(this.sunrise, this.sunset);

  int? sunrise;
  int? sunset;

  factory Sun.fromJson(Map<String, dynamic> json) => _$SunFromJson(json);

  Map<String, dynamic> toJson() => _$SunToJson(this);
}
