import 'package:json_annotation/json_annotation.dart';

part 'weather_description.g.dart';

@JsonSerializable()
class WeatherDescription {
  WeatherDescription(this.main, this.description, this.icon);

  String? main;
  String? description;
  String? icon;

  factory WeatherDescription.fromJson(Map<String, dynamic> json) => _$WeatherDescriptionFromJson(json);

  Map<String, dynamic> toJson() => _$WeatherDescriptionToJson(this);
}
