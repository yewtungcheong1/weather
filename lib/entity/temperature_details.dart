import 'package:json_annotation/json_annotation.dart';

part 'temperature_details.g.dart';

@JsonSerializable()
class TemperatureDetails {
  TemperatureDetails(this.temp, this.feelsLike, this.tempMin, this.tempMax, this.pressure, this.humidity);

  double? temp;
  @JsonKey(name: 'feels_like')
  double? feelsLike;
  @JsonKey(name: 'temp_min')
  double? tempMin;
  @JsonKey(name: 'temp_max')
  double? tempMax;
  double? pressure;
  double? humidity;

  factory TemperatureDetails.fromJson(Map<String, dynamic> json) => _$TemperatureDetailsFromJson(json);

  Map<String, dynamic> toJson() => _$TemperatureDetailsToJson(this);
}
