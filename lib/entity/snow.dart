import 'package:json_annotation/json_annotation.dart';

part 'snow.g.dart';

@JsonSerializable()
class Snow {
  Snow(this.oneHour, this.threeHour);

  @JsonKey(name: '1h')
  double? oneHour;
  @JsonKey(name: '3h')
  double? threeHour;

  factory Snow.fromJson(Map<String, dynamic> json) => _$SnowFromJson(json);

  Map<String, dynamic> toJson() => _$SnowToJson(this);
}
