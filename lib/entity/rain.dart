import 'package:json_annotation/json_annotation.dart';

part 'rain.g.dart';

@JsonSerializable()
class Rain {
  Rain(this.oneHour, this.threeHour);

  @JsonKey(name: '1h')
  double? oneHour;
  @JsonKey(name: '3h')
  double? threeHour;

  factory Rain.fromJson(Map<String, dynamic> json) => _$RainFromJson(json);

  Map<String, dynamic> toJson() => _$RainToJson(this);
}
