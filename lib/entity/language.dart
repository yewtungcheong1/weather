import 'package:flutter/material.dart';

class Language {
  Language(
    this.code,
    this.apiCode,
    this.locale,
  );

  String code;
  String apiCode;
  Locale locale;
}
