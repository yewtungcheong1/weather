
import 'package:json_annotation/json_annotation.dart';
part 'coordinate.g.dart';

@JsonSerializable()

class Coordinate {
  Coordinate(this.name, this.lat, this.lon);

  String? name;
  double? lat;
  double? lon;

  factory Coordinate.fromJson(Map<String, dynamic> json) => _$CoordinateFromJson(json);
  Map<String, dynamic> toJson() => _$CoordinateToJson(this);
}