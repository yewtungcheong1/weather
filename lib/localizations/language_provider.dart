import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:weather/entity/language.dart';

import 'my_localizations.dart';

class LanguageProvider {
  static LanguageProvider? _singleton;

  static LanguageProvider getSingleton() {
    _singleton ??= LanguageProvider();
    return _singleton!;
  }

  List<Language> langList = [
    Language("en","en", const Locale("en", '')),
    Language("zh","zh_CN", const Locale("zh", '')),
  ];

  Future<Locale?> setLocale(Language language) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    await _prefs.setString("LANGUAGE_CODE", language.code);
    return _locale(language);
  }

  Future<Locale?> getLocale() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    String? languageCode = _prefs.getString("LANGUAGE_CODE")??"en";
    return _locale(langList.singleWhere((e) => e.code == languageCode));
  }

  Locale? _locale(Language? language) {
    if (language == null) {
      return null;
    } else {
      return language.locale;
    }
  }

  String? getTranslated(BuildContext context, String? key, {bool isByBrand = false}) {
    return MyLocalizations.of(context)?.translate(key) ?? key;
  }
}
